# Mini Cookbook

A personal project, mini cook book for fun. Planning to use React and Bootstrap. 

Neither me (Anouschka) nor (Salih) have any experience with React, but I have experience with Angular. 
Both have experience in Javascript and webdevelopment. 


The goals:

Add ingredients (Ingredient, Unit(ml, dl, spoons, etc))
Add description (Simple text)
Add pictures    (Show in a swipe-able media gallery if possible)
Bonus: Scale up recipes, change the portions

This project will not be a fully-fledged project, but a small hobby cook-book to get used to React and Bootstrap. 